// Styles
import '@mdi/font/css/materialdesignicons.css';
// eslint-disable-next-line import/no-unresolved
import 'vuetify/styles';

// Vuetify
import { createVuetify } from 'vuetify';

export default createVuetify();
