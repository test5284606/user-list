export default {
  DELETE_USER(state, id) {
    const copyUser = [...state.data];
    const result = copyUser.filter((item) => item.id !== id);
    state.data = [...result];
  },
};
