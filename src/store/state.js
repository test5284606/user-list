export default {
  data: [
    { id: 1, name: 'Денис' },
    { id: 2, name: 'Эрнест' },
    { id: 3, name: 'Михаил' },
    { id: 4, name: 'Николай' },
    { id: 5, name: 'Игорь' },
    { id: 6, name: 'Марина' },
    { id: 7, name: 'Елизавета' },
    { id: 8, name: 'Наталья' },
    { id: 9, name: 'Алексей' },
  ],
};
